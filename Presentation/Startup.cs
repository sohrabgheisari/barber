﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation.AspNetCore;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Serialization;
using Barber.Core.Configuration;
using Barber.Core.Domain.Users;
using Barber.Core.Exceptions;
using Barber.Data;
using Barber.Infrastructure;
using Barber.Infrastructure.Framework;
using Barber.Infrastructure.Hubs;
using Barber.Infrastructure.Localizarions;
using Barber.Infrastructure.Security;
using Barber.Services.Users;
using Swashbuckle.AspNetCore.Swagger;
using NikanSms.Shared;

namespace Barber
{
    public class Startup
    {
        #region Fields

        private IServiceProvider _serviceProvider;

        #endregion

        #region Constructors

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsDevelopment())
            {
                builder.AddUserSecrets<Startup>();
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        #endregion

        #region Properties

        public IConfigurationRoot Configuration { get; }

        #endregion

        #region Configure

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper();
            services.AddKendo();
            services.AddSignalR();

            services.AddNikanSms(
             setupAction: options => Configuration.GetSection("NikanSms").Bind(options),
             activeSmsPanel: Configuration.GetValue<string>("NikanSms:ActiveSmsPanel"));

            services.Configure<AppConfig>(options => Configuration.GetSection("AppConfig").Bind(options));
            services.AddDbContext<AppDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<User, Role>(options =>
                {

                    options.Password.RequireDigit = false;
                    options.Password.RequiredLength = 5;
                    options.Password.RequireLowercase = false;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireUppercase = false;
                    options.User.RequireUniqueEmail = false;
                    options.SignIn.RequireConfirmedEmail = false;
                    options.SignIn.RequireConfirmedPhoneNumber = false;
                })
                .AddEntityFrameworkStores<AppDbContext>()
                .AddDefaultTokenProviders();

            services.AddDataProtection(configure =>
            {
                configure.ApplicationDiscriminator = "nikan.Base";
            });

            services.AddJsonLocalization(options =>
            {
                options.ResourcesPath = "Resources";
                options.GlobalResourceFileName = "global";
                options.AreasResourcePrefix = "areas";
            });

            services.Configure<RequestLocalizationOptions>(options =>
            {
                T Conf<T>(T cult) where T : CultureInfo
                {
                    cult.NumberFormat.NumberDecimalSeparator = ".";
                    cult.NumberFormat.NumberGroupSeparator = " ";
                    cult.NumberFormat.CurrencyDecimalSeparator = ".";
                    cult.DateTimeFormat.AMDesignator = "AM";
                    cult.DateTimeFormat.PMDesignator = "PM";
                    cult.DateTimeFormat.FullDateTimePattern = "yyyy/MM/dd HH:mm:ss.fff";

                    return cult;
                }

                var supportedCultures = new List<CultureInfo>
                {
                    Conf(new CustomPersianCultureInfo("fa-IR")),
                    Conf(new CustomPersianCultureInfo("fa")),
                    Conf(new CultureInfo("en-US")),
                    Conf(new CultureInfo("en-GB")),
                    Conf(new CultureInfo("en"))
                };

                options.DefaultRequestCulture = new RequestCulture("fa");
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
            });

            services.AddMemoryCache();
            services.AddMvc()
                .AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver())
                .AddViewLocalization()
                .AddDataAnnotationsLocalization()
                .AddFluentValidation(fvc =>
                    fvc.RegisterValidatorsFromAssemblyContaining<Startup>());


            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "1",
                    Title = "Base Project",
                });
            });

            AddAuthentication(services);

            //JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            //{
            //    Formatting = Formatting.None,
            //    ContractResolver = new DefaultContractResolver()
            //};

            new DependencyRegistrar().Register(services);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IServiceProvider serviceProvider, IOptions<RequestLocalizationOptions> localizationOptions)
        {
            _serviceProvider = serviceProvider;

            #region Errors

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/home/error");
                app.UseStatusCodePagesWithReExecute("/home/error/{0}");
            }

            #endregion

            #region Localization

            app.UseJsonLocalizer();
            app.UseRequestLocalization(localizationOptions.Value);

            #endregion

            UseAthentication(app);

            app.UseSignalR(route =>
            {
                route.MapHub<MessageHub>("/MessageHub");
            });
            GlobalHost.HubPipeline.RequireAuthentication();

            #region Static Files

            var provider = new FileExtensionContentTypeProvider();
            provider.Mappings[".apk"] = "application/vnd.android.package-archive";
            app.UseStaticFiles(new StaticFileOptions { ContentTypeProvider = provider });

            #endregion

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                  name: "areas",
                  template: "{area:exists}/{controller=Home}/{action=Index}/{id?}"
                );

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");


                routes.MapRoute(
                    name: "apis",
                    template: "api/{controller}/{action}/{id?}");

            });

            #region Swagger

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            #endregion

            app.Run(async (context) =>
            {
                context.Response.StatusCode = 404;
                await context.Response.WriteAsync("404 - Nothing found.");
            });
        }

        #endregion

        #region Authentication

        private SymmetricSecurityKey signingKey;

        private void AddAuthentication(IServiceCollection services)
        {
            services.AddAuthentication()
                    .AddCookie()
                    .AddJwtBearer(options =>
                    {
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuerSigningKey = true,
                            IssuerSigningKey = GetSigningKey(),
                            ValidateIssuer = true,
                            ValidIssuer = "http://nikan.com",
                            ValidateAudience = true,
                            ValidAudience = "api-users",
                            ValidateLifetime = false,
                            ClockSkew = TimeSpan.Zero
                        };
                    });

            services.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = new PathString("/account/login");
                options.AccessDeniedPath = new PathString("/account/login");
                options.LogoutPath = new PathString("/account/logout");
                options.Cookie.Name = "BarberCookie";//change this name for every project
            });
        }

        private void UseAthentication(IApplicationBuilder app)
        {
            #region Token Provider

            async Task<ClaimsIdentity> GetIdentity(string username, string password)
            {
                using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
                {
                    var um = serviceScope.ServiceProvider.GetService<UserManager>();
                    var rm = serviceScope.ServiceProvider.GetService<RoleManager<Role>>();
                    if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
                    {
                        var user = await um.FindByNameAsync(username);
                        if (user != null)
                        {
                            var valid = await um.VerifyChangePhoneNumberTokenAsync(user, password, username);
                            if (valid)
                            {
                                if (!user.IsActive)
                                    throw new NikanException("حساب کاربری شما غیر فعال شده است");

                                var jwtGenerator = new JwtTokenService(um, rm);
                                var claimIdentity = await jwtGenerator.GetUserClaims(user);
                                return claimIdentity;
                            }
                        }
                    }
                }

                // Credentials are invalid, or account doesn't exist
                return null;
            }

            app.UseSimpleTokenProvider(new TokenProviderOptions
            {
                Path = "/api/token",
                Audience = "api-users",
                Issuer = "http://nikan.com",
                Expiration = TimeSpan.FromDays(365),
                SigningCredentials = new SigningCredentials(GetSigningKey(), SecurityAlgorithms.HmacSha256),
                IdentityResolver = GetIdentity,
            });

            #endregion

            app.UseAuthentication();
        }

        private SymmetricSecurityKey GetSigningKey()
        {
            if (signingKey != null)
            {
                return signingKey;
            }

            var secretKey = Configuration.AsEnumerable().FirstOrDefault(x => x.Key == "AppConfig:JwtSecretKey").Value;
            if (!string.IsNullOrEmpty(secretKey))
            {
                signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey));
                return signingKey;
            }
            throw new Exception("SecretKey is not available");
        }

        #endregion
    }
}
