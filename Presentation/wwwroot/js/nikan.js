﻿/*-----------------------------------------------------------------
///   Copyright:		Copyright (c) 2018 - Nikan Inc. - All Rights Reserved
///   File:				nikanApp.js
///   Date:				2018-3-8
///   Purpose:			<Purpose>
///   Change history:	Developer			Date			Description
///						-					-				-
-----------------------------------------------------------------*/

"use strict";

function NikanApp(options) {
    $(document).on('focus', 'input[role="combobox"]', function () {
        $(this).select();
    });
    var that = this;
    var settings = {
        notify: {
            position: 'top-right',
            loaderBg: '#fff',
            hideAfter: 5000,
            stack: 6
        }
    };
    settings = $.extend(true, {}, settings, options);
    this.settings = settings;
    $(document).ajaxStart(function () {
        that.blockElement();
    })
        .ajaxStop(function () {
            that.unblockElement();
        })
        .ajaxError(function (jqXHR, exception) {
            debugger;
            console.log('خطا در عملیات:\n' + "status:" + exception.status + "responseText:" + exception.responseText);
        });

}

NikanApp.prototype.api = function (url, data, callback, blockElement, method) {

    var that = this;

    if (typeof method === 'undefined')
        method = "post";
    
    $.ajax({
        url: url,
        method: method,
        data: JSON.stringify(data),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        // beforeSend: function (xhr, settings) { //Include the bearer token in header
        //      xhr.setRequestHeader("Authorization", 'Bearer ' + localStorage.getItem('access_token'));
        //  },
        beforeSend: function () {
            that.blockElement(blockElement);
        },

        success: function (result) {

            if (typeof callback !== 'undefined') {
                callback(result);
            }
            else {
                console.error('POST ' + url + ' ' + 'Result is undefined');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            debugger
            if (errorThrown == "Unauthorized")
                window.location = "/Account/Login";

            if (errorThrown == "Forbidden")
                that.notify("error", "خطا", "کاربر گرامی,شما به عملیات مورد نظر دسترسی ندارید");

            // textStatus: null, "timeout", "error", "abort", or "parsererror"
            if (textStatus !== null) {
                console.error(`POST ${url} ${textStatus}: ${errorThrown}`);
            } else {
                console.error(`POST ${url} ${errorThrown}`);
            }
        },
        complete: function (jqXHR, textStatus) {
            // textStatus: "success", "notmodified", "nocontent", "error", "timeout", "abort", or "parsererror"
            that.unblockElement(blockElement);
        }
    });
};

NikanApp.prototype.BinaryApi = function (url, data, callback, blockElement) {

    var that = this;
    $.ajax({
        url: url,
        method: 'POST',
        data: data,
        dataType: "json",
        contentType: false,
        processData: false,
        // beforeSend: function (xhr, settings) { //Include the bearer token in header
        //      xhr.setRequestHeader("Authorization", 'Bearer ' + localStorage.getItem('access_token'));
        //  },
        beforeSend: function () {
            that.blockElement(blockElement);
        },

        success: function (msg) {
            if (typeof msg !== 'undefined') {
                if (typeof callback !== 'undefined') {
                    callback(msg);
                }
            } else {
                console.error('POST ' + url + ' ' + 'Result is undefined');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {

            if (errorThrown == "Unauthorized")
                window.location = "/Account/Login";

            if (errorThrown == "Forbidden")
                that.notify("error", "خطا", "کاربر گرامی,شما به عملیات مورد نظر دسترسی ندارید");

            // textStatus: null, "timeout", "error", "abort", or "parsererror"
            if (textStatus !== null) {
                console.error(`POST ${url} ${textStatus}: ${errorThrown}`);
            } else {
                console.error(`POST ${url} ${errorThrown}`);
            }
        },
        complete: function (jqXHR, textStatus) {
            // textStatus: "success", "notmodified", "nocontent", "error", "timeout", "abort", or "parsererror"
            that.unblockElement(blockElement);
        }
    });
}

NikanApp.prototype.blockElement = function (element) {
    var loadingMsg = "<h6> <img width='50px' height='50px' src='/images/loading.gif' />  لطفا منتظر بمانید ... </h6>";
    if (typeof element === 'undefined' || element == '') {
        $.blockUI({
            css: {
                border: 'none',
                padding: '10px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .8,
                color: '#000',
                background: '#fff'
            },
            message: loadingMsg,
            baseZ: 99999
        });
    }
    else {
        $('#' + element).block({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .8,
                color: '#000',
                background: '#fff'
            },
            message: loadingMsg
        });
    }
}

NikanApp.prototype.unblockElement = function (element) {
    if (!element) {
        $.unblockUI();
    }
    else {
        $('#' + element).unblock();
    }
}



NikanApp.prototype.notify = function (type, title, message, autoClose) {

    if (typeof autoClose === 'undefined' || autoClose == null)
        autoClose = true;

    $.toast({
        icon: type,
        heading: title,
        text: message,
        position: this.settings.notify.position,
        loaderBg: this.settings.notify.loaderBg,
        hideAfter: autoClose ? 5000 : false,
        stack: this.settings.notify.stack,
        allowToastClose: true,
    });
}

NikanApp.prototype.fixnotify = function (type, title, message) {

    $.toast({
        icon: type,
        heading: title,
        text: message,
        position: 'bottom-left',
        loaderBg: this.settings.notify.loaderBg,
        hideAfter:  false,
        stack: 10,
        allowToastClose: true,
        showHideTransition: 'slide'
    });
}

//modalSize:lg,sm,
function OpenModal(url, data, name, title, modalSize, callback) {

    modalSize = modalSize || 'lg';
    modalSize = 'modal-' + modalSize;

    var that = this;
    $('#' + name + ' .modal-body').html('');///important ,so that content of modal does not be cached next times

    $.get(url, data, function (result) {

        if (result.Success) {
            $('#' + name + ' .modal-body').html(result.Data);
            $('#' + name + ' .modal-title').html(title);

            //$('#' + name).modal({
            //    backdrop: 'static',
            //    keyboard: false,
            //    show: 'true'
            //});

            $('#' + name).modal({
                backdrop: 'static',
                keyboard: true
            },
                'show');

            $('#' + name + ' .modal-dialog').removeClass('modal-lg modal-sm modal-full');
            $('#' + name + ' .modal-dialog').addClass(modalSize);

            if (typeof callback !== 'undefined') {
                callback(result);
            }
        }
        else
            //that.notify("error", "خطا", result.Message);
            alert(result.message);
    });
}

function OpenStaticModal(content, name, title) {

    $('#' + name + ' .modal-body').html('');///important ,so that content of modal does not be cached next times

    $('#' + name + ' .modal-body').html(content);
    $('#' + name + ' .modal-title').html(title);
    $('#' + name).modal({
        backdrop: 'static',
        keyboard: false,
        show: 'true'
    });
    if (isModal) {
        $('#' + name + ' .modal-dialog').removeClass('fullScreenModalDialog');
        $('#' + name + ' .modal-content').removeClass('fullScreenModalContent');
    }
    else {
        $('#' + name + ' .modal-dialog').addClass('fullScreenModalDialog');
        $('#' + name + ' .modal-content').addClass('fullScreenModalContent');
    }
}


function CloseModal(name) {
    $('#' + name).modal('hide');
}

/////////////////////////////kendo grid/////////////////////////////////////////
function GridDataBinding(gridName) {
    var grid = $('#' + gridName).data('kendoGrid');
    return (grid.dataSource.page() - 1) * grid.dataSource.pageSize();
}
function IncrementRowNumber(index) {
    return ++index;
}
function RefreshKendoGrid(gridName, defaultPageSize) {

    if (gridName == null || gridName == '') return;
    var grid = $('#' + gridName).data('kendoGrid');

    grid.dataSource.page(1);
}
function GetDataByIdInKendoGridById(gridName, id) {
    var grid = $('#' + gridName).data('kendoGrid');
    var data = grid.dataSource.get(id);
    return data;
}
function KendoGridErrorHandler(e, gridName) {

    if (e.errors) {
        nikan.notify("error", "خطا", e.errors);

        //در ویرایش و حذف اینلاین گرید اگر خطایی رخ دهد ,گرید از حالت ویرایش خارج می شود وگرید رفرش نمی شود.
        var grid = $('#' + gridName).data('kendoGrid');
        grid.cancelChanges();

        //در حالت ویرایش اینلاین گرید اگر خطایی رخ دهد , همچنان در مد ویرایش می ماند
        //ولی اگر در حذف خطایی رخ دهد,ردیف از گرید حذف می شود
        //grid.one("dataBinding", function (e) {
        //    e.preventDefault();   // cancel grid rebind if error occurs                                             
        //});
    }
}

function GetImageByStatus(status) {

    if (status)
        return "<span class='fa fa-check'  aria-hidden='true' style=' font-size: 12px;'></span>";
    else
        return "<span class='fa fa-ban'  aria-hidden='true' style=' font-size: 12px;'></span>";
}

///////////////////////////////////kendo dropdown,combo//////////////////////

function GetSelectedValueOfCombo(dropName) {
    return $('#' + dropName).data('kendoComboBox').value();
}

function GetSelectedValueOfDropdown(dropName) {
    return $('#' + dropName).data('kendoDropDownList').value();
}


function MakeDropdownReadonly(dropName,readOnly) {
    return $('#' + dropName).data('kendoDropDownList').readonly(readOnly);
}
function MakeComboReadonly(dropName,readOnly) {
    return $('#' + dropName).data('kendoComboBox').readonly(readOnly);
}
function ClearComboText(comboName) {
    $("#" + comboName).data("kendoComboBox").text("");
}
/////////////////////////////////////Kendo Numerci////////////////
function SetValueOfNumericTextBox(txtBoxName, val) {
    var selector = $("[id$='" + txtBoxName + "']");
    var txtbox = selector.data("kendoNumericTextBox");
    txtbox.value(val);
}
