﻿using System.ComponentModel.DataAnnotations;

namespace Barber.Models.Account
{
    public class LoginModel
    {
        [Display(Name = "نام کاربری")]
        [Required(ErrorMessage = "msg.required")]
        public string Username { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "رمز عبور")]
        [Required(ErrorMessage = "msg.required")]
        public string Password { get; set; }

        [Display(Name = "مرا به خاطر داشته باش")]
        public bool RememberMe { get; set; }
    }
}
