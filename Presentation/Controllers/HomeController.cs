﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Barber.Infrastructure.UI;
using Barber.Services.Barber;
using ZarinpalSandbox;
using System.Threading.Tasks;

namespace Barber.Controllers
{
    public class HomeController : BasePublicController
    {
        #region Fields

        private readonly ILogger logger;
        private readonly IOrderService _orderService;
        private readonly IReserveService _reserveService;

        #endregion

        #region Constructors

        public HomeController(ILogger<HomeController> logger,
            IOrderService orderService, IReserveService reserveService)
        {
            this.logger = logger;
            _orderService = orderService;
            _reserveService = reserveService;
        }

        #endregion

        #region Actions 
        public IActionResult Index()
        {
            return RedirectToAction("Index", "Home", new { area = AreaNames.Admin });
           // return View();
        }

        public IActionResult Error(int? id)
        {
            var logBuilder = new System.Text.StringBuilder();

            var statusCodeReExecuteFeature = HttpContext.Features.Get<IStatusCodeReExecuteFeature>();
            logBuilder.AppendLine($"Error {id} for {Request.Method} {statusCodeReExecuteFeature?.OriginalPath ?? Request.Path.Value}{Request.QueryString.Value}\n");

            var exceptionHandlerFeature = HttpContext.Features.Get<IExceptionHandlerFeature>();
            if (exceptionHandlerFeature?.Error != null)
            {
                var exception = exceptionHandlerFeature.Error;
                logBuilder.AppendLine($"<h1>Exception: {exception.Message}</h1>{exception.StackTrace}");
            }

            foreach (var header in Request.Headers)
            {
                var headerValues = string.Join(",", value: header.Value);
                logBuilder.AppendLine($"{header.Key}: {headerValues}");
            }
            logger.LogError(logBuilder.ToString());

            if (id == null)
            {
                return View("Error");
            }

            switch (id.Value)
            {
                case 401:
                case 403:
                    return View("AccessDenied");
                case 404:
                    return View("NotFound");

                default:
                    return View("Error");
            }
        }

        public async Task<IActionResult> OnlinePayment(long id)
        {
            if (HttpContext.Request.Query["Status"] != "" &&
                HttpContext.Request.Query["Status"].ToString().ToLower() == "ok" &&
                HttpContext.Request.Query["Authority"] != "")
            {
                string authority = HttpContext.Request.Query["Authority"].ToString();
                var order = await _orderService.GetOrderById(id);
                var reserve = await _reserveService.GetReserveById(order.ReserveId);
                var payment = new Payment((int)order.PrePaid);
                var res =await payment.Verification(authority);             
                if (res.Status == 100)
                {
                    order.IsFinally = true;
                    order.RefId = res.RefId;
                    order.TrackingCode = authority;
                    reserve.PaymentConfirmation = true;
                    await _orderService.UpdateOrder(order);
                    await _reserveService.UpdateReserve(reserve);
                    ViewBag.RefId = res.RefId;
                    ViewBag.code = authority;
                    ViewBag.Done = true;
                    return View();
                }

            }
            else
            {
                ViewBag.Done = false;
                return View();
            }

            return NotFound();
        }
        #endregion

        #region Utilities



        #endregion
    }
}
