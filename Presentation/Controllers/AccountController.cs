﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Barber.Models.Account;
using Barber.Services.Users;

namespace Barber.Controllers
{
    public class AccountController : BasePublicController
    {
        #region Fields

        private readonly SignInManager signInManager;
        private readonly IStringLocalizer localizer;
        #endregion

        #region Constructors

        public AccountController(SignInManager signInManager, IStringLocalizer<LoginModel> localizer)
        {
            this.signInManager = signInManager;
            this.localizer = localizer;
        }

        #endregion

        #region Actions


        public async Task<IActionResult> Login(string returnUrl = null)
        {
           
            if (!Url.IsLocalUrl(returnUrl))
            {
                returnUrl = null;
            }

            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel model, string returnUrl = null)
        {

            if (!Url.IsLocalUrl(returnUrl))
            {
                returnUrl = null;
            }

            ViewData["ReturnUrl"] = returnUrl;

            if (ModelState.IsValid)
            {
                var user = await signInManager.UserManager.FindByNameAsync(model.Username.Trim());
                if (user != null)
                {
                    if (!user.IsActive)
                    {
                        NotifyError(message: localizer["user.msg.inactiveUser"].Value);
                    }
                    var result=await signInManager.PasswordSignInAsync(user, model.Password, model.RememberMe, lockoutOnFailure: false);
                    if (result.Succeeded)
                    {
                        if (!string.IsNullOrEmpty(returnUrl))
                        {
                            return LocalRedirect(returnUrl);
                        }
                        else
                        {
                            return Redirect("/admin/Home/index");
                        }
                    }
                }
                else if (model.Username == "admin")
                {
                    return Redirect("/install");
                }

                NotifyError(message: localizer["login.msg.login-failed"].Value);
            }
            else
            {
                NotifyError(message: localizer["login.msg.login-invalid"].Value);
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(IdentityConstants.ApplicationScheme);
            return LocalRedirect("/home");
        }

        public IActionResult AccessDenied()
        {
            return View();
        }
        #endregion

        #region Utilities



        #endregion
    }
}
