﻿using AutoMapper;
using Barber.Areas.Admin.Models;
using Barber.Areas.Admin.Models.Order;
using Barber.Areas.Admin.Models.Reserve;
using Barber.Controllers;
using Barber.Core.Domain.Barber;
using Barber.Core.Enums;
using Barber.Core.Infrastructure;
using Barber.Services.Barber;
using Barber.Services.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZarinpalSandbox;

namespace Barber.Areas.Admin.Controllers
{
    [Route("api/[controller]")]
    [Route("[area]/[controller]")]
    [Authorize(Roles = UserRoleNames.User)]
    public class OrderController : BaseAdminController
    {

        #region Fields
        private readonly IAppContext _appContext;
        private readonly IMapper _mapper;
        private readonly IServiceService _serviceService;
        private readonly IOrderService _orderService;
        private readonly IOrderDetailService _orderDetailService;
        private readonly IReserveService _reserveService;
        private readonly IReservedServiceService _reservedServiceService;
        private readonly UserManager _userManager;
        #endregion

        #region Constructors
        public OrderController(IAppContext appContext,
            IStringLocalizer<OrderModel> userLocalizer,
            IMapper mapper, ILogger<OrderController> logger,
            IServiceService serviceService, IOrderService orderService,
            IOrderDetailService orderDetailService, IReserveService reserveService,
            IReservedServiceService reservedServiceService, UserManager userManager
            ) : base(logger, userLocalizer)
        {
            _appContext = appContext;
            _mapper = mapper;
            _serviceService = serviceService;
            _orderService = orderService;
            _orderDetailService = orderDetailService;
            _reserveService = reserveService;
            _reservedServiceService = reservedServiceService;
            _userManager = userManager;
        }
        #endregion

        [HttpGet("[action]")]
        public IActionResult Index()
        {
            return View();
        }

        #region Apis

        [HttpPost("[action]")]
        public Task<ResponseState<string>> SubmitOrder([FromBody] OrderModel orderModel)
        {
            return TryCatch(async () =>
            {
                var res = await InsertOrder(orderModel);
                if (res.Success)
                {
                    return Success(data: $"{Request.Scheme}://{Request.Host}/api/Order/Payment/{res.Data}");

                }
                return Error(data:"",message:"خطا در ثبت درخواست");
            });
        }


        [HttpGet("[action]/{id}")]
        [AllowAnonymous]
        public async Task<IActionResult> Payment(long id)
        {
            var order = await _orderService.GetOrderById(id);
            if (order == null)
            {
                return NotFound();
            }
            var user = await _userManager.GetById(order.ClientId);
            var payment = new Payment((int)order.PrePaid);
            var res = await payment.PaymentRequest($"پرداخت فاکتور شماره {order.Id}",
                $"{Request.Scheme}://{Request.Host}/Home/OnlinePayment/{order.Id}", user.Email, user.PhoneNumber);
            if (res.Status == 100)
            {
                return Redirect("https://sandbox.zarinpal.com/pg/StartPay/" + res.Authority);
            }
            else
            {
                return BadRequest();
            }

        }


        [HttpGet("[action]")]
        public async Task<ResponseState<List<ReserveViewModel>>> GetReservedList([FromQuery] bool confirmed = true)
        {
            var order = await _orderService.GetOrderByUserId(_appContext.User.Id, confirmed);
            List<ReserveViewModel> reserves = new List<ReserveViewModel>();
            foreach (var item in order)
            {
                var model = _mapper.Map<ReserveViewModel>(item);
                model.FillRemainFields();
                List<ServiceViewModel> services = new List<ServiceViewModel>();
                foreach (var reserve in item.Reserve.ReservedServices)
                {
                    services.Add(_mapper.Map<ServiceViewModel>(await _serviceService.GetServicesByIds(reserve.ServiceId)));
                }
                model.Services = services;
                reserves.Add(model);
            }
            return Success(data: reserves);

        }
        #endregion

        #region Utilities
        private Task<ResponseState<long>> InsertOrder( OrderModel orderModel)
        {
            return TryCatch(async () =>
            {
                #region PreparationData

                var orderDetails = GetOrderDetails(orderModel.ServicesIds);
                //ثبت درخواست
                Order order = new Order()
                {
                    ClientId = _appContext.User.Id,
                    PayerName = orderModel.PayerName,
                    CreateDate = DateTime.Now,
                    DateTimeStamp = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(),
                    Price = orderDetails.Sum(x => x.Price),
                    PrePaid = orderModel.PrePaid,
                    IsFinally = false,
                };
                await _orderService.InsertOrder(order);

                //ثبت رزرو
                Reserve reserve = new Reserve()
                {
                    OrderId = order.Id,
                    ClientId = _appContext.User.Id,
                    PrePaid = orderModel.PrePaid,
                    PaymentConfirmation = false,
                    Date = orderModel.DateTimeStamp.TimeStampToDateTime(),
                    DateTimeStamp = orderModel.DateTimeStamp
                };
                await _reserveService.InsertReserve(reserve);

                //بروزرسانی درخواست
                order.ReserveId = reserve.Id;
                await _orderService.UpdateOrder(order);

                //ثبت ریز درخواست ها
                orderDetails.ForEach(x => x.OrderId = order.Id);
                await _orderDetailService.InsertOrderDetails(orderDetails);

                //ثبت روابط رزرو و سرویس
                var reservedServiceList = GetReservedServiceList(orderDetails, reserve.Id);
                await _reservedServiceService.InsertReservedService(reservedServiceList);

                #endregion

                return Success(data:order.Id);
            });
        }

        private List<OrderDetail> GetOrderDetails(List<long> orders)
        {
            List<OrderDetail> list = new List<OrderDetail>();
            var services = _serviceService.GetServicesWithIds(orders).Result;
            foreach (var service in services)
            {
                list.Add(new OrderDetail()
                {
                    Price = service.DefaultCost.Value,
                    ServiceId = service.Id
                });
            }
            return list;
        }

        private List<ReservedService> GetReservedServiceList(List<OrderDetail> ordersDetails, long reserveId)
        {
            List<ReservedService> list = new List<ReservedService>();
            ordersDetails.ForEach(x =>
            {
                list.Add(new ReservedService() { ReserveId = reserveId, ServiceId = x.ServiceId });
            });
            return list;
        }
        #endregion
    }
}