﻿using AutoMapper;
using Barber.Areas.Admin.Models;
using Barber.Areas.Admin.Models.Service;
using Barber.Controllers;
using Barber.Core.Enums;
using Barber.Core.Infrastructure;
using Barber.Services.Barber;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Barber.Areas.Admin.Controllers
{
    [Route("api/[controller]")]
    [Route("[area]/[controller]")]
    [Authorize(Roles = UserRoleNames.User)]
    public class ServiceController : BaseAdminController
    {
        #region Fields
        private readonly IAppContext _appContext;
        private readonly IMapper _mapper;
        private readonly IServiceService _serviceService;
        #endregion

        #region Constructors
        public ServiceController(IAppContext appContext,
            IStringLocalizer<ServiceModel> userLocalizer,
            IMapper mapper, ILogger<ServiceController> logger,
            IServiceService serviceService
            ) : base(logger, userLocalizer)
        {
            _appContext = appContext;
            _mapper = mapper;
            _serviceService = serviceService;
        }
        #endregion

        #region Action
        [HttpGet("[action]")]
        public IActionResult Index()
        {
            return View();
        }
        #endregion

        #region Apis
        [HttpGet("[action]")]
        public Task<ResponseState<List<ServiceModel>>> GetAllServices()
        {
            return TryCatch(async () =>
            {
                var entities = _serviceService.GetAllServiceAsQueryable().ToList();
                var model = _mapper.Map<List<ServiceModel>>(entities);
                model.ForEach(x => x.HasChild = _serviceService.ServicesHasChilde(x.Id).Result);
                return Success(model);
            });
        }
        #endregion

        #region Utilities
    
        #endregion
    }
}