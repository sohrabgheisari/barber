﻿using Barber.Controllers;
using Barber.Core.Enums;
using Barber.Core.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;

namespace Barber.Areas.Admin.Controllers
{
    [Route("api/[controller]")]
    [Route("[area]/[controller]")]
    [Authorize(Roles = UserRoleNames.Admin)]

    public class HomeController : BaseAdminController
    {
        #region Fields
        private readonly IAppContext _appContext;
        #endregion

        #region Constructors

        public HomeController(
          IStringLocalizer<HomeController> localizer,
          ILogger<HomeController> logger,
           IAppContext appContext
          ) : base(logger, localizer)
        {
            _appContext = appContext;
        }


        #endregion

        #region Actions

        [HttpGet("[Action]")]
        public IActionResult Index()
        {
            return View();
        }


        #endregion

        #region Utilities



        #endregion
    }
}