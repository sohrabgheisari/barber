﻿
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Barber.Controllers;
using Barber.Areas.Admin.Models;
using Barber.Areas.Admin.Models.User;
using Barber.Core.Domain.Users;
using Barber.Infrastructure.Security;
using Barber.Services.Users;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Barber.Core.Infrastructure;
using Barber.Core.Enums;
using Building.Infrastructure;
using Microsoft.AspNetCore.Identity;
using NikanSms.Shared;

namespace Barber.Areas.Admin.Controllers
{
    [Route("api/[controller]")]
    [Route("[area]/[controller]")]
    public class UserController : BaseAdminController
    {
        #region Fields
        private readonly IAppContext _appContext;
        private readonly UserManager _userManager;
        private readonly IMapper _mapper;
        private readonly SignInManager _signInManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly INikanSmsSender _smsService;

        #endregion

        #region Constructors
        public UserController(IAppContext appContext,
            IStringLocalizer<UserModel> userLocalizer,
            IMapper mapper,
            ILogger<UserController> logger,
            UserManager userManager,
            SignInManager signInManager,
            RoleManager<Role> roleManager,
            INikanSmsSender smsService
            ) : base(logger, userLocalizer)
        {

            _appContext = appContext;
            _userManager = userManager;
            _mapper = mapper;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _smsService = smsService;
        }
        #endregion

        #region Actions

        [HttpGet("[action]")]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("[action]")]
        [Authorize(Roles = UserRoleNames.Admin)]
        public async Task<ResponseState<List<UserIndexModel>>> Data_Read()
        {
            return await TryCatch(async () =>
            {
                var result = await _userManager.GetAll();
                var models = _mapper.Map<List<UserIndexModel>>(result);
                models.ForEach(m => m.Roles = RoleExtension.ConvertRolesToLocalizedName(result.FirstOrDefault(n => n.Id == m.Id).UserRoleNames, Localizer));
                return Success(models);
            });
        }


        [HttpGet("[action]/{id}")]
        public Task<ResponseState<string>> EditUser(long id)
        {
            return TryCatch(async () =>
            {
                var user = await _userManager.GetByIdWithRoles(id);
                var model = _mapper.Map<EditUserModel>(user);
                ViewBag.Roles = RoleExtension.ConvertRolesToSelectList(_roleManager, Localizer);
                var view = await PartialViewToString(PartialView("EditUser", model), ControllerContext);
                return Success(data: view);
            });
        }

        [HttpPost("[action]")]
        public Task<ResponseState<long>> EditUser(EditUserModel model)
        {
            return TryCatch(async () =>
            {
                var user = await _userManager.GetById(model.Id);
                user.IsActive = model.IsActive;
                await _userManager.Update(user, model.UserRoleNames, true);
                return Success(user.Id, message: Localizer["msg.save-succeed"].Value);
            });
        }

        [HttpGet("[action]")]
        public Task<ResponseState<string>> Create()
        {
            return TryCatch(async () =>
            {
                var model = new UserModel() { IsActive = true };
                ViewBag.Roles = RoleExtension.ConvertRolesToSelectList(_roleManager, Localizer);
                var view = await PartialViewToString(PartialView("CreateUser", model), ControllerContext);
                return Success(data: view);
            });
        }

        [HttpPost("[action]")]
        public Task<ResponseState<long>> Create(UserModel model)
        {
            return TryCatch(async () =>
            {
                var entity = _mapper.Map<User>(model);

                await _userManager.Insert(entity, model.Password, model.UserRoleNames);
                return Success(entity.Id, message: Localizer["msg.save-succeed"].Value);
            });
        }

        [HttpGet("[action]")]
        public Task<ResponseState<string>> ChangePassword()
        {
            return TryCatch(async () =>
            {
                var model = new ChangePasswordModel();
                var view = await PartialViewToString(PartialView("ChangePassword", model), ControllerContext);
                return Success(data: view);
            });
        }

        [HttpPost("[action]")]
        public Task<ResponseState<long>> ChangePassword(ChangePasswordModel model)
        {
            return TryCatch(async () =>
            {
                if (ModelState.IsValid)
                {
                    var user = await _userManager.GetUserAsync(User);
                    if (user == null)
                    {
                        return Error(user.Id, message: "کاربری یافت نشد");
                    }
                    var Result = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
                    if (Result.Succeeded)
                    {
                        await _signInManager.RefreshSignInAsync(user);
                        return Success(user.Id, message: "رمز عبور با موفقیت تغییر یافت");
                    }
                    return Error(user.Id, message: "رمز عبور قدیم اشتباه است");
                }
                else
                {
                    var msg = string.Join(" | ", ModelState.Values
                           .SelectMany(v => v.Errors)
                           .Select(e => e.ErrorMessage));
                    return Error<long>(0, message: msg);
                }
            });
        }


        [HttpGet("[action]/{id}")]
        public Task<ResponseState<string>> SetNewPassword(long id)
        {
            return TryCatch(async () =>
            {
                var model = new SetNewPasswordModel() { Id = id };
                var view = await PartialViewToString(PartialView("SetNewPass", model), ControllerContext);

                return Success(data: view);
            });
        }

        [HttpPost("[action]")]
        public Task<ResponseState<long>> SetNewPassword(SetNewPasswordModel model)
        {
            return TryCatch(async () =>
            {
                var user = await _userManager.GetById(model.Id);
                string passwordHash = _userManager.PasswordHasher.HashPassword(user, model.NewPassword);
                user.PasswordHash = passwordHash;
                var result = await _userManager.UpdateAsync(user);
                if (result.Succeeded)
                {
                    return Success(user.Id, message: Localizer["msg.save-succeed"].Value);
                }
                return Error(user.Id, message: "عملیات با شکست مواجه گردید");
            });
        }

        [HttpPost("[action]")]
        [AllowAnonymous]
        public Task<DefaultResponseState> Login([FromQuery]string mobile)
        {
            return TryCatch(async () =>
            {
                if (ModelState.IsValid)
                {
                    var user = await _userManager.FindByNameAsync(mobile);
                    if (user != null)
                    {
                        string code = await _userManager.GenerateChangePhoneNumberTokenAsync(user, mobile);
                        //Dictionary<string, string> dictionary = new Dictionary<string, string>();
                        //dictionary.Add("code", code.ToString());
                        //dictionary.Add("token", "USpGHvfw0qq");
                        //SmsSendResult smsResult = await _smsSender.SendSms(long.Parse(model.Mobile), "2024", dictionary);
                        INikanSmsSendResult smsResult = await _smsService.SendSms(long.Parse(mobile),$"کد تایید شما: {code}");


                        if (!smsResult.IsSucceeded)
                        {
                            return Error("خطا در ارسال اسمس");
                        }
                        return Success();
                    }
                    else
                    {
                        User newUser = new User()
                        {
                            UserName = mobile,
                            IsActive = true,
                            PhoneNumber =mobile
                        };

                        await _userManager.CreateAsync(newUser);
                        await _userManager.AddToRoleAsync(newUser, UserRoleNames.User);
                        string code = await _userManager.GenerateChangePhoneNumberTokenAsync(newUser, mobile);
                        //Dictionary<string, string> dictionary = new Dictionary<string, string>();
                        //dictionary.Add("code", code.ToString());
                        //dictionary.Add("token", "USpGHvfw0qq");
                        //SmsSendResult smsResult = await _smsSender.SendSms(long.Parse(model.Mobile), "2024", dictionary);

                        INikanSmsSendResult smsResult = await _smsService.SendSms(long.Parse(mobile), $"کد تایید شما: {code}");

                        if (!smsResult.IsSucceeded)
                        {
                            return Error("خطا در ارسال اسمس");
                        }
                        return Success();
                    }
                }
                return Error("شماره موبایل اشتباه است");

            });
        }
        #endregion

        #region Utilities

        #endregion
    }
}
