﻿using Barber.Areas.Admin.Models.Order;
using FluentValidation;

namespace Barber.Areas.Admin.Validators.Order
{
    public class OrderModelValidator:AbstractValidator<OrderModel>
    {
        public OrderModelValidator()
        {
            CascadeMode = CascadeMode.StopOnFirstFailure;

            RuleFor(x => x)
                .Must(services => services.ServicesIds.Count > 0)
                .WithMessage("انتخاب سرویس اجباری است");

            RuleFor(x => x.DateTimeStamp)
                .NotEmpty().WithMessage("تاریخ رزرو اجباری است")
                .NotNull().WithMessage("تاریخ رزرو اجباری است")
                .Must(time => time.ToString().Length == 10)
                .WithMessage("تاریخ رزرو معتبر نمی باشد");

            RuleFor(x => x.PrePaid)
               .NotNull().WithMessage("پیش پرداخت اجباری است")
               .NotEmpty().WithMessage("پیش پرداخت اجباری است")
               .GreaterThan(0).WithMessage("تاریخ رزرو معتبر نمی باشد");

            RuleFor(x => x.PayerName)
                .MaximumLength(50)
                .WithMessage("حداکثر طول مجاز برای نام پرداخت کننده ۵۰ کاراکتر است");
        }
    }
}
