﻿using FluentValidation;
using Microsoft.Extensions.Localization;
using Barber.Areas.Admin.Models.User;
using Barber.Services.Users;

namespace Barber.Areas.Admin.Validators.User
{
    public class EditUserModelValidator : AbstractValidator<EditUserModel>
    {
        private readonly UserManager userManager;
        public EditUserModelValidator(IStringLocalizer<EditUserModel> localizer, UserManager userManager)
        {
            this.userManager = userManager;
            CascadeMode = CascadeMode.StopOnFirstFailure;
            RuleFor(x => x.UserRoleNames)
                .NotEmpty().WithMessage(string.Format(localizer["msg.required"].Value, localizer["user.field.Role"].Value));

        }
    }
}
