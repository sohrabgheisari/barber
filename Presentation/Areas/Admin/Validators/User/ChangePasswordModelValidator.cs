﻿using FluentValidation;
using Microsoft.Extensions.Localization;
using Barber.Areas.Admin.Models.User;

namespace Barber.Areas.Admin.Validators.User
{
    public class ChangePasswordModelValidator : AbstractValidator<ChangePasswordModel>
    {

        public ChangePasswordModelValidator(IStringLocalizer<ChangePasswordModel> localizer)
        {

            CascadeMode = CascadeMode.StopOnFirstFailure;
            RuleFor(x => x.OldPassword)
                .NotEmpty().WithMessage(string.Format(localizer["msg.required"].Value, localizer["user.changePassword.field.OldPassword"].Value));

            RuleFor(x => x.NewPassword)
               .NotEmpty().WithMessage(string.Format(localizer["msg.required"].Value, localizer["user.changePassword.field.NewPassword"].Value));

            RuleFor(x => x.ConfirmPassword)
             .NotEmpty().WithMessage(string.Format(localizer["msg.required"].Value, localizer["user.changePassword.field.ConfirmNewPassword"].Value))
             .Equal(m => m.NewPassword).WithMessage(string.Format(localizer["msg.Equal"].Value, localizer["user.changePassword.field.NewPassword"].Value,
             localizer["user.changePassword.field.ConfirmNewPassword"].Value));

        }
    }
}
