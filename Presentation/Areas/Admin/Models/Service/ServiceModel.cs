﻿namespace Barber.Areas.Admin.Models.Service
{
    public class ServiceModel:BaseModel
    {
        public ServiceModel()
        {

        }
        /// <summary>
        /// عنوان سرویس
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// قیمت پیش فرض
        /// </summary>
        public long? DefaultCost { get; set; }
        /// <summary>
        /// مدت زمان کار به دقیقه
        /// </summary>
        public int? Duration { get; set; }
        public long? ParentServiceId { get; set; }
        public long? RootServiceId { get; set; }
        public int Level { get; set; }
        public bool HasChild { get; set; }

    }
}
