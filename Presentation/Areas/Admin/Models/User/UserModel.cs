﻿using System.Collections.Generic;

namespace Barber.Areas.Admin.Models.User
{
    public class UserModel : BaseModel
    {
        public UserModel()
        {
            UserRoleNames = new List<string>();
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public bool IsActive { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public List<string> UserRoleNames { get; set; }
    }
}