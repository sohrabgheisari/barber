﻿using System.Collections.Generic;

namespace Barber.Areas.Admin.Models.User
{
    public class EditUserModel:BaseModel
    {
        public List<string> UserRoleNames { get; set; }
        public int? InternalCode { get; set; }
        public bool IsActive { get; set; }
    }
}
