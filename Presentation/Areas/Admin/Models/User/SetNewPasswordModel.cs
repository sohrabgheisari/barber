﻿namespace Barber.Areas.Admin.Models.User
{
    public class SetNewPasswordModel:BaseModel
    {
        public SetNewPasswordModel()
        {
                
        }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
    }
}
