﻿using System.Collections.Generic;

namespace Barber.Areas.Admin.Models.Order
{
    public class OrderModel
    {
        public List<long> ServicesIds { get; set; }
        public long DateTimeStamp { get; set; }
        public long PrePaid { get; set; }
        public string PayerName { get; set; }

    }
}
