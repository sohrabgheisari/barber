﻿namespace Barber.Areas.Admin.Models.Reserve
{
    public class ServiceViewModel
    {
        public string Title { get; set; }
        public long? DefaultCost { get; set; }
        public int? Duration { get; set; }
    }
}
