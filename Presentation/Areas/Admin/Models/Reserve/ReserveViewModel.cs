﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Barber.Areas.Admin.Models.Reserve
{
    public class ReserveViewModel:BaseModel
    {
        public ReserveViewModel()
        {

        }
        public DateTime CreateDate { get; set; }
        public long Price { get; set; }
        public long PrePaid { get; set; }
        public string TrackingCode { get; set; }
        public long? RefId { get; set; }
        public string PayerName { get; set; }

        public long Remain { get; set; }
        public List<ServiceViewModel> Services { get; set; }

        public void FillRemainFields()
        {
            Remain = Price - PrePaid;
        }
    }
}
