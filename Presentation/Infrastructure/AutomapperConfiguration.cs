﻿using AutoMapper;
using Barber.Areas.Admin.Models.Reserve;
using Barber.Areas.Admin.Models.Service;
using Barber.Areas.Admin.Models.User;
using Barber.Core.Domain.Barber;
using Barber.Core.Domain.Users;
using Barber.Core.Infrastructure;
using System;

namespace Barber.Infrastructure
{
    public class DateTimeToPersianDateConverter : ITypeConverter<DateTime, string>
    {
        public string Convert(DateTime source, string destination, ResolutionContext context)
        {
            return source.ToPersianDateTime();
        }
    }
    public class NullableDateTimeToPersianDateConverter : ITypeConverter<DateTime?, string>
    {
        public string Convert(DateTime? source, string destination, ResolutionContext context)
        {
            return source.ToPersianDateTime();
        }
    }
    public class TimeSpanToStringConverter : ITypeConverter<TimeSpan, string>
    {
        public string Convert(TimeSpan source, string destination, ResolutionContext context)
        {
            return source.ToPersianTime();
        }
    }
    public class NullableTimeSpanToStringConverter : ITypeConverter<TimeSpan?, string>
    {
        public string Convert(TimeSpan? source, string destination, ResolutionContext context)
        {
            return source.ToPersianTime();
        }
    }
    public class AutomapperConfiguration : Profile
    {

        public AutomapperConfiguration()
        {
            //convert null to empty string
            CreateMap<string, string>().ConvertUsing(s => s ?? string.Empty);

            // این تنظیم سراسری هست و به تمام خواص زمانی اعمال می‌شود
            CreateMap<DateTime, string>().ConvertUsing(new DateTimeToPersianDateConverter());
            CreateMap<DateTime?, string>().ConvertUsing(new NullableDateTimeToPersianDateConverter());
            CreateMap<TimeSpan, string>().ConvertUsing(new TimeSpanToStringConverter());
            CreateMap<TimeSpan?, string>().ConvertUsing(new NullableTimeSpanToStringConverter());

            #region User

            CreateMap<User, UserModel>().ReverseMap();
            CreateMap<User, EditUserModel>().ReverseMap();
            CreateMap<User, UserIndexModel>();

            #endregion

           

            #region Service
            CreateMap<Service, ServiceModel>().ReverseMap();
            CreateMap<Service, ServiceViewModel>().ReverseMap();
            #endregion

            #region Order
            CreateMap<Order, ReserveViewModel>().ReverseMap();
            #endregion
        }
    }
}
