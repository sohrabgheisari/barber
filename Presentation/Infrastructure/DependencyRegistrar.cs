﻿using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using Barber.Infrastructure.Framework;
using Barber.Infrastructure.Security;
using Barber.Services.Security;
using Barber.Services.Users;
using Barber.Core.Infrastructure;
using Microsoft.AspNetCore.Http;
using Barber.Services.Caching;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Barber.Services.Barber;

namespace Barber.Infrastructure
{
    public class DependencyRegistrar
    {
        public void Register(IServiceCollection services)
        {
            // App
            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IStaticCacheManager, MemoryCacheManager>();
            services.AddScoped<ICacheManager, MemoryCacheManagerWrapper>();
            services.AddScoped<IAppContext, AppContext>();
            services.AddScoped<ScriptManager>();
            services.AddScoped<StyleManager>();
            services.AddScoped<UserManager>();
            services.AddScoped<SignInManager>();
            services.AddScoped<IEncryptionService, EncryptionService>();
            services.AddScoped<JwtTokenService>();
            services.AddScoped<IEncryptionService, EncryptionService>();
            services.AddSingleton<PartialViewResultExecutor>();

            services.AddScoped(typeof(IServiceService), typeof(ServiceService));
            services.AddScoped(typeof(IOrderService), typeof(OrderService));
            services.AddScoped(typeof(IOrderDetailService), typeof(OrderDetailService));
            services.AddScoped(typeof(IReserveService), typeof(ReserveService));
            services.AddScoped(typeof(IReservedServiceService), typeof(ReservedServiceService));
        }
    }
}
