﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace Barber.Infrastructure.Hubs
{
    [Authorize]
    public class MessageHub : Hub
    {
      
        public  Task SendMessageToAll(string message)
        {
            return Clients.All.SendAsync("ReceiveMessage", message);
        }

        public Task SendMessageToUser(string userId,string message)
        {
            return Clients.User(userId).SendAsync("ReceiveMessage", message);
        }
       
    }
}
