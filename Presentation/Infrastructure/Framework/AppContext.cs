﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Barber.Core.Configuration;
using Barber.Core.Domain.Users;
using Barber.Core.Infrastructure;
using Barber.Services.Users;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Barber.Core.Enums;

namespace Barber.Infrastructure.Framework
{
    public class AppContext : IAppContext
    {
        #region Fields

        private readonly IHttpContextAccessor _contextAccessor;
        private readonly UserManager _userManager;
        private User cachedUser = null;
      
        #endregion

        #region Constructors

        public AppContext(IHttpContextAccessor contextAccessor,
            UserManager userManager,
            IOptions<AppConfig> appConfig)
        {
            _contextAccessor = contextAccessor;
            _userManager = userManager;
        }

        #endregion

        #region Properties

        public User User
        {
            get
            {
                if (cachedUser == null)
                {
                    cachedUser = Task.Run(() =>
                    {
                        return GetUser();
                    }).Result;
                }

                return cachedUser;
            }
        } 

        #endregion

        #region Methods

        public void ClearCache()
        {  
            cachedUser = null;
        }

        public void EnsureGuestCookieRemoved()
        {
            if (_contextAccessor.HttpContext.Request.Cookies.ContainsKey(Constants.GuestUser))
            {
                _contextAccessor.HttpContext.Response.Cookies.Delete(Constants.GuestUser);
            }
        }

        public bool TryGetGuestUserId(out long guestId)
        {
            if (_contextAccessor.HttpContext.Request.Cookies.TryGetValue(Constants.GuestUser, out string guestIdStr) &&
                long.TryParse(guestIdStr, out guestId))
            {
                return true;
            }
            else
            {
                guestId = 0;
                return false;
            }
        }

        #endregion

        #region Utilities

        private async Task<User> GetUser()
        {
            if (cachedUser != null)
            {
                return cachedUser;
            }

            if (_contextAccessor.HttpContext == null)
            {
                return null;
            }

            var principal = _contextAccessor.HttpContext.User;
            if (principal != null)
            {
                var username = _userManager.NormalizeKey(principal.Identity.Name);
                var user = await _userManager.Users.FirstOrDefaultAsync(x => x.NormalizedUserName == username);
                if (user != null)
                {
                    await _userManager.ConvertUserRoleToRole(new List<User>() { user });
                    cachedUser = user;
                    return user;
                }
            } 
            return null;
        }
        private string GenerateUsername(int maxLength)
        {
            var rng = RandomNumberGenerator.Create();
            // Generate a cryptographic random number
            var buff = new byte[maxLength];
            rng.GetBytes(buff);

            // Return a Base64 string representation of the random number
            var code = Convert.ToBase64String(buff);
            var username = string.Empty;

            foreach (var c in code)
            {
                if (char.IsLetterOrDigit(c))
                {
                    username += c.ToString();
                }
            }

            return username;
        }

        #endregion
    }
}
