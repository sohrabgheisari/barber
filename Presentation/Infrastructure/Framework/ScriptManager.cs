﻿namespace Barber.Infrastructure.Framework
{
    public class ScriptManager
    {
        public FileList Top { get; } = new FileList();
        public FileList Bottom { get; } = new FileList();
    }
}
