﻿using Microsoft.AspNetCore.Builder;

namespace Barber.Infrastructure.Localizarions
{
    public static class JsonLocalizerMiddlewareExtensions
    {
        public static IApplicationBuilder UseJsonLocalizer(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<JsonLocalizerMiddleware>();
        }
    }
}
