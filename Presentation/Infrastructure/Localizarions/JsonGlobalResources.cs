﻿using System;
using System.Collections.Concurrent;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Barber.Infrastructure.Localizarions
{
    public class JsonGlobalResources
    {
        #region Fields

        private readonly ConcurrentDictionary<string, Lazy<JObject>> _resources = new ConcurrentDictionary<string, Lazy<JObject>>();
        private readonly IHostingEnvironment _app;
        private readonly string _resourcesRelativePath;
        private readonly string _globalName;
        private readonly string _areaName;
        private readonly RequestCulture _defaultCulture;

        #endregion

        #region Constructors

        public JsonGlobalResources(IHostingEnvironment hostingEnvironment, IOptions<JsonLocalizationOptions> options, RequestCulture defaultCulture)
        {
            if (options == null)
                throw new ArgumentNullException(nameof(options));

            _app = hostingEnvironment ?? throw new ArgumentNullException(nameof(hostingEnvironment));
            _globalName = options.Value?.GlobalResourceFileName ?? "global";
            _areaName = options.Value?.AreasResourcePrefix ?? "areas";
            _defaultCulture = defaultCulture ?? throw new ArgumentNullException(nameof(defaultCulture));

            _resourcesRelativePath = options.Value?.ResourcesPath ?? string.Empty;
            if (!string.IsNullOrEmpty(_resourcesRelativePath))
            {
                _resourcesRelativePath = _resourcesRelativePath.Replace(Path.AltDirectorySeparatorChar, '.').Replace(Path.DirectorySeparatorChar, '.');
            }
        }

        #endregion

        #region Methods
        
        public JObject GetGlobalResources(CultureInfo culture)
        {
            var cultureSuffix = "." + culture.Name;
            cultureSuffix = cultureSuffix == "." ? "" : cultureSuffix;

            if (LocalizerUtil.IsChildCulture(_defaultCulture.UICulture, culture) || LocalizerUtil.IsChildCulture(culture, _defaultCulture.UICulture))
            {
                cultureSuffix = "";
            }
            
            var cacheName = "global";
            cacheName = cacheName + (string.IsNullOrEmpty(cultureSuffix) ? ".default" : cultureSuffix);

            var lazyJObjectGetter = new Lazy<JObject>(() =>
            {
                var resourceBaseName = string.IsNullOrEmpty(_resourcesRelativePath) ? _app.ApplicationName : _app.ApplicationName + "." + _resourcesRelativePath + "." + _globalName;
                var resourceFileLocations = LocalizerUtil.ExpandPaths(resourceBaseName, _app.ApplicationName).ToList();

                string resourcePath = null;
                foreach (var resourceFileLocation in resourceFileLocations)
                {
                    resourcePath = resourceFileLocation + cultureSuffix + ".json";
                    if (File.Exists(resourcePath))
                    {
                        break;
                    }
                    else
                    {
                        resourcePath = null;
                    }
                }

                if (resourcePath == null)
                {
                    return null;
                }

                try
                {
                    var resourceFileStream = new FileStream(resourcePath, FileMode.Open, FileAccess.Read, FileShare.Read, 4096, FileOptions.Asynchronous | FileOptions.SequentialScan);
                    using (resourceFileStream)
                    {
                        var resourceReader = new JsonTextReader(new StreamReader(resourceFileStream, Encoding.UTF8, detectEncodingFromByteOrderMarks: true));
                        using (resourceReader)
                        {
                            return JObject.Load(resourceReader);
                        }
                    }
                }
                catch (Exception e)
                {
                    return null;
                }
            }, LazyThreadSafetyMode.ExecutionAndPublication);

            lazyJObjectGetter = _resources.GetOrAdd(cacheName, lazyJObjectGetter);
            return lazyJObjectGetter.Value;
        }

        public JObject GetAreaResources(CultureInfo culture, string areaName)
        {
            if (string.IsNullOrEmpty(areaName?.Trim()))
            {
                throw new ArgumentNullException(nameof(areaName));
            }

            var cultureSuffix = "." + culture.Name;
            cultureSuffix = cultureSuffix == "." ? "" : cultureSuffix;

            if (LocalizerUtil.IsChildCulture(_defaultCulture.UICulture, culture) || LocalizerUtil.IsChildCulture(culture, _defaultCulture.UICulture))
            {
                cultureSuffix = "";
            }

            var areaSuffix = $".{areaName}";

            var cacheName = $"areas{areaSuffix}";
            cacheName = cacheName + (string.IsNullOrEmpty(cultureSuffix) ? ".default" : cultureSuffix);

            var lazyJObjectGetter = new Lazy<JObject>(() =>
            {
                var resourceBaseName = string.IsNullOrEmpty(_resourcesRelativePath) ? _app.ApplicationName : _app.ApplicationName + "." + _resourcesRelativePath + "." + _areaName;
                var resourceFileLocations = LocalizerUtil.ExpandPaths(resourceBaseName, _app.ApplicationName).ToList();

                string resourcePath = null;
                foreach (var resourceFileLocation in resourceFileLocations)
                {
                    resourcePath = resourceFileLocation + areaSuffix + cultureSuffix + ".json";
                    if (File.Exists(resourcePath))
                    {
                        break;
                    }
                    else
                    {
                        resourcePath = null;
                    }
                }

                if (resourcePath == null)
                {
                    return null;
                }

                try
                {
                    var resourceFileStream = new FileStream(resourcePath, FileMode.Open, FileAccess.Read, FileShare.Read, 4096, FileOptions.Asynchronous | FileOptions.SequentialScan);
                    using (resourceFileStream)
                    {
                        var resourceReader = new JsonTextReader(new StreamReader(resourceFileStream, Encoding.UTF8, detectEncodingFromByteOrderMarks: true));
                        using (resourceReader)
                        {
                            return JObject.Load(resourceReader);
                        }
                    }
                }
                catch (Exception e)
                {
                    return null;
                }
            }, LazyThreadSafetyMode.ExecutionAndPublication);

            lazyJObjectGetter = _resources.GetOrAdd(cacheName, lazyJObjectGetter);
            return lazyJObjectGetter.Value;
        }

        public void ClearCache()
        {
            _resources.Clear();
        }

        #endregion
    }
}
