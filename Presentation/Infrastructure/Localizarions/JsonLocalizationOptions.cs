﻿namespace Barber.Infrastructure.Localizarions
{
    public class JsonLocalizationOptions
    {
        public string ResourcesPath { get; set; }

        public string GlobalResourceFileName { get; set; }

        public string AreasResourcePrefix { get; set; }
    }
}
