﻿namespace Barber.Services.Caching
{
    public static class CacheKeys
    {
        #region Customer

        public const string Person_Pattern = "^customer\\..";

        private const string Person_Base = "customer.";

        public const string Person_Search = Person_Base + "search-{0}";

        #endregion
    }
}
