﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Barber.Services.Security
{
    public class EncryptionService : IEncryptionService
    {
        public string CreateHash(byte[] data)
        {
            using (var shaHash = SHA256.Create())
            {
                var hashData = shaHash.ComputeHash(data);
                var sBuilder = new StringBuilder();
                for (int i = 0; i < hashData.Length; i++)
                {
                    sBuilder.Append(hashData[i].ToString("x2"));
                }

                return sBuilder.ToString();
            }
        }

        public string CreatePasswordHash(string password)
        {
            return CreateHash(Encoding.UTF8.GetBytes(password));
            throw new NotImplementedException();
        }

        public bool VerifyPassword(string password, string hash)
        {
            var hashOfInput = CreatePasswordHash(password);
            var comparer = StringComparer.OrdinalIgnoreCase;

            return comparer.Compare(hashOfInput, hash) == 0;
        }
    }
}
