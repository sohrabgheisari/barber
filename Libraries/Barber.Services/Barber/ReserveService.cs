﻿using Barber.Core.Domain.Barber;
using Barber.Data;
using Barber.Services.Infrastructure;
using System.Threading.Tasks;

namespace Barber.Services.Barber
{
    public class ReserveService : BaseService, IReserveService
    {
        public ReserveService(AppDbContext context) : base(context)
        {
        }

        public Task<Reserve> GetReserveById(long entityId)
        {
            return GetById<Reserve>(entityId);
        }

        public Task InsertReserve(Reserve entity)
        {
            Insert(entity);
            return Save();
        }

        public Task UpdateReserve(Reserve entity)
        {
            Update(entity);
            return Save();
        }
    }
}
