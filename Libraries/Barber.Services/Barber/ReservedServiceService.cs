﻿using Barber.Core.Domain.Barber;
using Barber.Data;
using Barber.Services.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Barber.Services.Barber
{
    public class ReservedServiceService : BaseService, IReservedServiceService
    {
        public ReservedServiceService(AppDbContext context) : base(context)
        {
        }

        public Task InsertReservedService(ReservedService entity)
        {
            Insert(entity);
            return Save();
        }

        public Task InsertReservedService(List<ReservedService> entities)
        {

            Insert(entities);
            return Save();
        }
    }
}
