﻿using Barber.Core.Domain.Barber;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Barber.Services.Barber
{
    public interface IOrderDetailService
    {
        Task InsertOrderDetails(List<OrderDetail> entities);
    }
}
