﻿using Barber.Core.Domain.Barber;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Barber.Services.Barber
{
   public interface IServiceService
    {
        IQueryable<Service> GetAllServiceAsQueryable();
        Task<List<Service>> GetServicesWithIds(List<long> list);
        Task<Service> GetServicesByIds(long entityId);
        Task<bool> ServicesHasChilde(long entityId);
    }
}
