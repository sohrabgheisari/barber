﻿using Barber.Core.Domain.Barber;
using Barber.Data;
using Barber.Services.Infrastructure;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Barber.Services.Barber
{
    public class OrderDetailService : BaseService, IOrderDetailService
    {
        public OrderDetailService(AppDbContext context) : base(context)
        {
        }

        public Task InsertOrderDetails(List<OrderDetail> entities)
        {
            Insert(entities);
            return Save();
        }
    }
}
