﻿using Barber.Core.Domain.Barber;
using System.Threading.Tasks;

namespace Barber.Services.Barber
{
    public interface IReserveService
    {
        Task InsertReserve(Reserve entity);
        Task UpdateReserve(Reserve entity);
        Task<Reserve> GetReserveById(long entityId);
    }
}
