﻿using Barber.Core.Domain.Barber;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Barber.Services.Barber
{
    public interface IOrderService
    {
        Task InsertOrder(Order entity);
        Task UpdateOrder(Order entity);
        Task<Order> GetOrderById(long entityId);
        Task<List<Order>> GetOrderByUserId(long userId,bool confirmed);
    }
}
