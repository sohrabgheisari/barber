﻿using Barber.Core.Domain.Barber;
using Barber.Data;
using Barber.Services.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Barber.Services.Barber
{
    public class ServiceService : BaseService, IServiceService
    {
        public ServiceService(AppDbContext context) : base(context)
        {
        }

        public IQueryable<Service> GetAllServiceAsQueryable()
        {
            return Table<Service>();
        }

        public Task<Service> GetServicesByIds(long entityId)
        {
            return GetById<Service>(entityId);
        }

        public Task<List<Service>> GetServicesWithIds(List<long> list)
        {
            return Table<Service>().Where(x => list.Contains(x.Id)).ToListAsync();
        }

        public Task<bool> ServicesHasChilde(long entityId)
        {
            var service = Table<Service>().FirstOrDefault(x => x.ParentServiceId == entityId);
            return service != null ? Task.FromResult(true) : Task.FromResult(false);
        }
    }
}
