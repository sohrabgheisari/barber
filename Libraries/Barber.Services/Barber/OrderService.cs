﻿using Barber.Core.Domain.Barber;
using Barber.Data;
using Barber.Services.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Barber.Services.Barber
{
    public class OrderService : BaseService, IOrderService
    {
        public OrderService(AppDbContext context) : base(context)
        {
        }

        public Task<Order> GetOrderById(long entityId)
        {
            return GetById<Order>(entityId);
        }

        public Task<List<Order>> GetOrderByUserId(long userId, bool confirmed)
        {
            return Table<Order>().Where(x => x.ClientId == userId && x.IsFinally == confirmed).OrderByDescending(x=>x.CreateDate)
                .Include(x => x.Reserve.ReservedServices).ToListAsync();
        }

        public Task InsertOrder(Order entity)
        {
            Insert(entity);
            return Save();
        }

        public Task UpdateOrder(Order entity)
        {

            Update(entity);
            return Save();
        }
    }
}
