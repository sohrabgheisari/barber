﻿using Barber.Core.Domain.Barber;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Barber.Services.Barber
{
    public interface IReservedServiceService
    {
        Task InsertReservedService(ReservedService entity);
        Task InsertReservedService(List<ReservedService> entities);
    }
}
