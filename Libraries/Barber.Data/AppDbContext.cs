﻿using Barber.Core.Domain.Barber;
using Barber.Core.Domain.Users;
using Barber.Data.Infrastructure;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Data.Common;

namespace Barber.Data
{

    public class AppDbContext :
      IdentityDbContext<
          User,
          Role,
          long,
          IdentityUserClaim<long>,
          IdentityUserRole<long>,
          IdentityUserLogin<long>,
          IdentityRoleClaim<long>,
          IdentityUserToken<long>>
    {
        public AppDbContext(DbContextOptions options)
            : base(options)
        {
        }

        #region Sets
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<Reserve> Reserves { get; set; }
        public DbSet<ReservedService> ReservedServices { get; set; }

        #endregion

        public virtual StoredProcQuery CreateStoredProcQuery()
        {
            return new StoredProcQuery();
        }

        public virtual DbCommand CreateCommand()
        {
            return Database.GetDbConnection().CreateCommand();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(AppDbContext).Assembly);
        }
    }
}

