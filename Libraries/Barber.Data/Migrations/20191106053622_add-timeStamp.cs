﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Barber.Data.Migrations
{
    public partial class addtimeStamp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "DateTimeStamp",
                table: "Reserve",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "DateTimeStamp",
                table: "Order",
                nullable: false,
                defaultValue: 0L);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateTimeStamp",
                table: "Reserve");

            migrationBuilder.DropColumn(
                name: "DateTimeStamp",
                table: "Order");
        }
    }
}
