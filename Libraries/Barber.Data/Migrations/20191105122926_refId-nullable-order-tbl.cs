﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Barber.Data.Migrations
{
    public partial class refIdnullableordertbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "RefId",
                table: "Order",
                nullable: true,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "RefId",
                table: "Order",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);
        }
    }
}
