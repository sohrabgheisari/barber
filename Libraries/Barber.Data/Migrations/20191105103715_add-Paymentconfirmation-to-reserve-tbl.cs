﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Barber.Data.Migrations
{
    public partial class addPaymentconfirmationtoreservetbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "PaymentConfirmation",
                table: "Reserve",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PaymentConfirmation",
                table: "Reserve");
        }
    }
}
