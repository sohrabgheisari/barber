﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Barber.Data.Migrations
{
    public partial class addPrePaidtoordertbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "PrePaid",
                table: "Order",
                nullable: false,
                defaultValue: 0L);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PrePaid",
                table: "Order");
        }
    }
}
