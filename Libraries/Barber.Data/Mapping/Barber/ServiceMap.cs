﻿using Barber.Core.Domain.Barber;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Barber.Data.Mapping.Barber
{
    internal class ServiceMap : IEntityTypeConfiguration<Service>
    {
        public void Configure(EntityTypeBuilder<Service> entity)
        {
            entity.ToTable("Service");

            entity.HasKey(x => x.Id);
        }
    }
}
