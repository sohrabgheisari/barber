﻿using Barber.Core.Domain.Barber;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Barber.Data.Mapping.Barber
{
    internal class ReservedServiceMap : IEntityTypeConfiguration<ReservedService>
    {
        public void Configure(EntityTypeBuilder<ReservedService> entity)
        {
            entity.ToTable("ReservedService");

            entity.HasKey(x => x.Id);

            entity.HasOne(x => x.Reserve)
                .WithMany(x => x.ReservedServices)
                .HasForeignKey(x => x.ReserveId)
                .OnDelete(DeleteBehavior.Restrict);


            entity.HasOne(x => x.Service)
                .WithMany(x => x.ReservedServices)
                .HasForeignKey(x => x.ServiceId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
