﻿using Barber.Core.Domain.Barber;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Barber.Data.Mapping.Barber
{
    internal class OrderMap : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> entity)
        {
            entity.ToTable("Order");

            entity.HasKey(x => x.Id);

            entity.Property(x => x.PayerName)
                .HasMaxLength(50);
        }
    }
}
