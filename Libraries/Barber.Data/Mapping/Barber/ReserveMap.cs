﻿using Barber.Core.Domain.Barber;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Barber.Data.Mapping.Barber
{
    internal class ReserveMap : IEntityTypeConfiguration<Reserve>
    {
        public void Configure(EntityTypeBuilder<Reserve> entity)
        {
            entity.ToTable("Reserve");

            entity.HasKey(x => x.Id);


            entity.HasOne<Order>(x => x.Order)
            .WithOne(x => x.Reserve)
            .HasForeignKey<Reserve>(x => x.OrderId)
            .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
