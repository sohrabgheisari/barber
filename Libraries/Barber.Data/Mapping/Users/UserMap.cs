﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Barber.Core.Domain.Users;

namespace Barber.Data.Mapping.Users
{
    class UserMap : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> entity)
        {
         
            entity.Property(e => e.Email)
                .HasMaxLength(256);

            entity.Property(e => e.EmailConfirmed);

            entity.Property(e => e.FirstName)
                .HasMaxLength(50);

            entity.Property(e => e.LastName)
              .IsRequired(false)
              .HasMaxLength(50);

            entity.Property(e => e.LockoutEnabled);

            entity.Property(e => e.LockoutEnd);

            entity.Property(e => e.NormalizedEmail)
                .HasMaxLength(256);

            entity.Property(e => e.NormalizedUserName)
                .HasMaxLength(256);

            entity.Property(e => e.PhoneNumber)
                .IsRequired(false);

            entity.Property(e => e.PhoneNumberConfirmed);

          
            entity.Property(e => e.UserName)
                .IsRequired()
                .HasMaxLength(256);

            entity.Property(e => e.Address)
             .HasMaxLength(1000);

            entity.Property(e => e.Gender);

            entity.Property(e => e.IsActive);

        }
    }
}
