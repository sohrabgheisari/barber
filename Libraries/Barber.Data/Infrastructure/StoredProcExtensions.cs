﻿namespace Barber.Data.Infrastructure
{
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;

    public static class StoredProcExtensions
    {
        public static StoredProcQuery StoredProc(this AppDbContext context, string storedProcName, bool prependDefaultSchema = true)
        {
            if (prependDefaultSchema)
            {
                var schemaName = context.Model.Relational().DefaultSchema;
                if (schemaName != null)
                {
                    storedProcName = $"{schemaName}.{storedProcName}";
                }
            }

            StoredProcQuery query = context.CreateStoredProcQuery();
            query.Context = context;
            query.Name = storedProcName;
            return query;
        }

        public static StoredProcQuery WithParam(this StoredProcQuery query, string name, object value, ParameterDirection direction = ParameterDirection.Input, DbType? dbType = null, int size = 0)
        {
            DbCommand cmd = query.Context.CreateCommand();

            DbParameter parameter = cmd.CreateParameter();
            parameter.ParameterName = name;
            parameter.Direction = direction;
            parameter.Size = size;

            if (direction == ParameterDirection.Input || direction == ParameterDirection.InputOutput)
            {
                parameter.Value = value ?? DBNull.Value;
            }

            if (dbType.HasValue)
            {
                parameter.DbType = dbType.Value;
            }

            query.Parameters.Add(parameter);
            return query;
        }

        public static StoredProcQuery WithParams<TEntity>(this StoredProcQuery query, TEntity entity, Expression<Func<TEntity, object>> expression)
            where TEntity : class, new()
        {
            DbCommand cmd = query.Context.CreateCommand();
            List<DbParameter> parameters = new List<DbParameter>();

            void Add(string name, object value)
            {
                DbParameter parameter = cmd.CreateParameter();
                parameter.ParameterName = name;
                parameter.Value = value ?? DBNull.Value;
                parameters.Add(parameter);
            }

            if (expression.Body is UnaryExpression unary)
            {
                if (unary.Operand is MethodCallExpression methodExpression)
                {
                    object value = expression.Compile().Invoke(entity);
                    Add(methodExpression.Method.Name, value);
                }
                else
                {
                    string name = ((MemberExpression)unary.Operand).Member.Name;
                    object value = expression.Compile().Invoke(entity);
                    Add(name, value);
                }
            }
            else if (expression.Body is MemberExpression member)
            {
                object value = expression.Compile().Invoke(entity);
                Add(member.Member.Name, value);
            }
            else
            {
                PropertyInfo[] properties = expression.Body.Type.GetProperties().ToArray();

                foreach (PropertyInfo item in properties)
                {
                    object value = entity.GetType().GetProperty(item.Name).GetValue(entity);
                    Add(item.Name, value);
                }
            }

            query.Parameters.AddRange(parameters);
            return query;
        }
    }
}
