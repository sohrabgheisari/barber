﻿using Microsoft.AspNetCore.Identity;
using Barber.Core.Domain.Users;
using System;
using System.Linq;

namespace Barber.Data
{
    public class DBInitializer
    {
        public static void Initialize(AppDbContext context)
        {
            DBInitializer initializer = new DBInitializer();
            initializer.Seed(context);
        }

        public void Seed(AppDbContext context)
        {
            context.Database.EnsureCreated();

            if (context.Roles.Any())
            {
                // DB has been seeded
                return;
            }

            using (var transaction = context.Database.BeginTransaction())
            {

                var adminRole = new Role
                {
                    Name = "Admin",
                    NormalizedName = "ADMIN",
                    ConcurrencyStamp = Guid.NewGuid().ToString(),
                };
                var userRole = new Role
                {
                    Name = "User",
                    NormalizedName = "USER",
                    ConcurrencyStamp = Guid.NewGuid().ToString(),
                };

                context.Set<Role>().Add(adminRole);
                context.Set<Role>().Add(userRole);
                context.SaveChanges();

                User user1 = new User
                {
                    ConcurrencyStamp = Guid.NewGuid().ToString(),
                    PasswordHash = "AQAAAAEAACcQAAAAEC5mbvNHbg2ctI8aMJj5ZNn6t7tu05Gw6iTTqXZLlAblXRNdD4xm3LmnamgqtDn1Tw==",
                    SecurityStamp = "5440e31d-b341-4046-b8f2-628f3ef54747",
                    UserName = "Admin",
                    NormalizedUserName = "Admin".Normalize().ToUpperInvariant(),
                    FirstName = "کاربر",
                    LastName = "ادمین",
                    Email = "admin@yourwebsite.com",
                    NormalizedEmail = "admin@yourwebsite.com".Normalize().ToUpperInvariant(),
                    IsActive = true,
                };
                context.Set<User>().AddRange(new[] { user1 });
                context.SaveChanges();

                context.UserRoles.AddRange(new[]
                {
                    new IdentityUserRole<long>
                    {
                        RoleId = adminRole.Id,
                        UserId = user1.Id
                    }
                });
                context.SaveChanges();

                transaction.Commit();
            }
        }
    }
}
