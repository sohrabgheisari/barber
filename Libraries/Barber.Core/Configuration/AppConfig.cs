﻿namespace Barber.Core.Configuration
{
    public class AppConfig
    {
        public string JwtSecretKey { get; set; }
        public int CacheLifeTimeMinutes { get; set; }
    }
}
