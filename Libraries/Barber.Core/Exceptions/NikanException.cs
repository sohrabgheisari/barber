﻿using System;
using System.Collections.Generic;

namespace Barber.Core.Exceptions
{
    public class NikanException : Exception
    {
        private ICollection<string> _relatedErrors;

        public NikanException()
            : base()
        {
        }

        public NikanException(string message)
            : base(message)
        {
        }

        public NikanException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public NikanException(string message, Exception innerException, List<string> relatedErrors)
            : base(message, innerException)
        {
            RelatedErrors = relatedErrors;
        }

        public NikanException(string message, List<string> relatedErrors)
          : base(message)
        {
            RelatedErrors = relatedErrors;
        }

        public object[] Arguments { get; set; }

        public virtual ICollection<string> RelatedErrors
        {
            get => _relatedErrors ?? (_relatedErrors = new List<string>());
            set => _relatedErrors = value;
        }
    }
}
