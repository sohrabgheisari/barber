﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace Barber.Core.Domain.Users
{
    public class User : IdentityUser<long>
    {
        public User()
        {
            UserRoleNames = new List<string>();
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool Gender { get; set; }//0 Male  1 Femail
        public string Address { get; set; }
        public bool IsActive { get; set; }

        [NotMapped]
        public List<string> UserRoleNames { get; set; }

        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }
    }
}
