﻿using System;
using System.Collections.Generic;

namespace Barber.Core.Domain.Barber
{
    public class Order:BaseEntity
    {
        public Order()
        {

        }
        public long ReserveId { get; set; }
        public long ClientId { get; set; }  
        public DateTime CreateDate { get; set; }
        public long DateTimeStamp { get; set; }
        public long Price { get; set; }
        public long PrePaid { get; set; }
        public bool IsFinally { get; set; } = false;
        /// <summary>
        /// کد پیگیری
        /// </summary>
        public string TrackingCode { get; set; }
        public long? RefId { get; set; }
        public string PayerName { get; set; }
        public virtual Reserve Reserve { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
