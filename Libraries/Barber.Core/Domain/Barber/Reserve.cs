﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Barber.Core.Domain.Barber
{
    public class Reserve:BaseEntity
    {
        public Reserve()
        {

        }
        /// <summary>
        /// تاریخ رزرو
        /// </summary>
        public DateTime Date { get; set; }
        public long DateTimeStamp { get; set; }
        /// <summary>
        /// مبلغ پیش پرداخت
        /// </summary>
        public long PrePaid { get; set; }
        /// <summary>
        /// شناسه مشتری
        /// </summary>
        public long ClientId { get; set; }
        public long OrderId { get; set; }
        public bool PaymentConfirmation { get; set; } = false;
        public virtual Order Order { get; set; }
        public virtual ICollection<ReservedService> ReservedServices { get; set; }

    }
}
