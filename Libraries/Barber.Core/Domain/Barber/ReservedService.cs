﻿namespace Barber.Core.Domain.Barber
{
    public class ReservedService:BaseEntity
    {
        public ReservedService()
        {

        }
        public long ReserveId { get; set; }
        public long ServiceId { get; set; }
        public virtual Service Service { get; set; }
        public virtual Reserve Reserve { get; set; }
    }
}
