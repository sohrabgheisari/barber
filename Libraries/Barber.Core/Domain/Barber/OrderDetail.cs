﻿namespace Barber.Core.Domain.Barber
{
    public class OrderDetail:BaseEntity
    {
        public OrderDetail()
        {

        }

        public long OrderId { get; set; }
        public long ServiceId { get; set; }
        public long Price { get; set; }
        public Order Order { get; set; }
        public virtual Service Service { get; set; }
    }
}
