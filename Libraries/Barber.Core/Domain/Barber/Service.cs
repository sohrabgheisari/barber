﻿using System.Collections.Generic;

namespace Barber.Core.Domain.Barber
{
    public class Service:BaseEntity
    {
        public Service()
        {

        }
        /// <summary>
        /// عنوان سرویس
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// قیمت پیش فرض
        /// </summary>
        public long? DefaultCost { get; set; }
        /// <summary>
        /// مدت زمان کار به دقیقه
        /// </summary>
        public int? Duration { get; set; }
        public long? ParentServiceId { get; set; }
        public long? RootServiceId { get; set; }
        public int Level { get; set; }

        public virtual ICollection<Reserve> Reserves { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
        public virtual ICollection<ReservedService> ReservedServices { get; set; }


    }
}
