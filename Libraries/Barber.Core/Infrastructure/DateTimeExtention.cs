﻿using MD.PersianDateTime.Core;
using System;
using System.Globalization;

namespace Barber.Core.Infrastructure
{
    public static class DateTimeExtentions
    {
        public static string ToPersianTime(this TimeSpan ts)
        {
            return ts.Hours.ToString().PadLeft(2, '0') + ":" + ts.Minutes.ToString().PadLeft(2, '0');
        }
        public static string ToPersianTime(this TimeSpan? ts)
        {
            return ts.HasValue ? ToPersianTime(ts.Value) : "";
        }
        public static string ToPersianDate(this DateTime dateTime)
        {
            PersianCalendar pc = new PersianCalendar();
            try
            {
                return string.Format("{0}/{1}/{2}", pc.GetYear(dateTime).ToString().PadLeft(4, '0'),
                    pc.GetMonth(dateTime).ToString().PadLeft(2, '0'),
                    pc.GetDayOfMonth(dateTime).ToString().PadLeft(2, '0'));
            }
            catch
            {
                return "";
            }
        }
        public static string ToPersianDateTime(this DateTime dateTime)
        {
            try
            {
                return string.Format("{0} {1}:{2}", dateTime.ToPersianDate(), dateTime.Hour.ToString().PadLeft(2, '0'),
                    dateTime.Minute.ToString().PadLeft(2, '0'));
            }
            catch
            {
                return "";
            }
        }
        public static string ToPersianDate(this DateTime? dateTime)
        {
            if (dateTime != null)
            {
                return ToPersianDate(dateTime.Value);
            }
            return string.Empty;
        }
        public static string ToPersianDateTime(this DateTime? dateTime)
        {
            if (dateTime != null)
            {
                return ToPersianDateTime(dateTime.Value);
            }
            return string.Empty;
        }
        public static DateTime PersianDateToMiladi(this string persiandate)
        {
            PersianDateTime persianDate = PersianDateTime.Parse(persiandate);
            DateTime miladiDate = persianDate.ToDateTime();
            return miladiDate;
        }

        public static DateTime TimeStampToDateTime(this long timestamp)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(timestamp).ToLocalTime();
            return dtDateTime;
        }
    }
}
